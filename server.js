const app = require('express')();
const domains = require('./country-by-domain-tld');
const port = 3001;
const bodyParser = require('body-parser');

//app.use(express.static(path.join(__dirname, '/front/build')));

app.use(bodyParser.json());

app.post('/users', function(req, res, next) {
  var b = req.body.userName;
  var i = 0;
  var filteredDomains = [];
  while (b.length - i > 3) {
    console.log('while');
    var req = '.' + b.slice(b.length - i - 2, b.length - i);
    console.log('req', req);
    domains.map(domain => {
      if (domain.tld == req) {
        console.log('pushed');
        filteredDomains.push(
          Object.assign(domain, {
            address: b.slice(0, b.length - i - 2) + req + '/' + b.slice(b.length - i)
          })
        );
      }
    });
    i++;
  }
  res.json(filteredDomains);
});

app.listen(port, err => {
  if (err) {
    return console.log('Some errors');
  }

  console.log(`Server is listening on ${port}`);
});
