import React, { Component } from 'react';
import './styles.css';

class Domain extends Component {
  render() {
    return (
      <div className="domainsList container">
        <div className="row justify-content-md-center">
          <div className="col ">
            <h4 className="domain__note">{this.props.index}</h4>
          </div>
          <div className="col ">
            <h4 className="domain__note">{this.props.domain}</h4>
          </div>
          <div className="col ">
            <h4 className="domain__note">{this.props.country}</h4>
          </div>
        </div>
      </div>
    );
  }
}

export default Domain;
