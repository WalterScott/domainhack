import React, { Component } from 'react';
import Domain from '../domain';
import './styles.css';

class DomainsList extends Component {
  render() {
    return (
      <div className="container">
        <div className="row justify-content-md-center">
          <div className="col ">
            <h4 className="domainsList__h4">#</h4>
          </div>
          <div className="col ">
            <h4 className="domainsList__h4">Domain</h4>
          </div>
          <div className="col ">
            <h4 className="domainsList__h4">Country</h4>
          </div>
        </div>
        {this.props.domains.map((domain, index) => (
          <Domain index={index + 1} domain={domain.address} country={domain.country} />
        ))}
      </div>
    );
  }
}

export default DomainsList;
