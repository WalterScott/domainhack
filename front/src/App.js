import React, { Component } from 'react';
import DomainsList from './components/domainsList';
import './app.css';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      filteredDomains: [],
      isFilteredDomains: false
    };
  }

  handleChange = e => {
    this.setState(
      {
        input: e.target.value
      },
      () => this.getDomains()
    );
  };

  getDomains = () => {
    console.log(this.state.input, ' -');
    fetch('/users', {
      method: 'post',
      body: JSON.stringify({
        userName: this.state.input
      }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(res => {
        this.setState({ filteredDomains: res });
        this.state.filteredDomains.length
          ? this.setState({ isFilteredDomains: true })
          : this.setState({ isFilteredDomains: false });
      });
  };

  render() {
    return (
      <div className="app">
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col ">
              <h2 className="app__title">DomainHack.Me</h2>
            </div>
          </div>
          <div className="row justify-content-md-center">
            <div className="col ">
              <h3 className="app__subtitle">The world's finest Domain-Hacking Tool</h3>
            </div>
          </div>
          <div className="row justify-content-md-center">
            <div className="col">
              <input
                className="app__input"
                type="text"
                placeholder="e.g. testinput"
                onChange={this.handleChange}
                name="userName"
              />
            </div>
          </div>
          {this.state.isFilteredDomains ? <DomainsList domains={this.state.filteredDomains} /> : ''}
          <div className="row justify-content-md-center app__footer">
            <div className="col">
              <h3 className="app__h3"># Domain-Hack, WTF?!</h3>
              <p>
                A <a href="https://en.wikipedia.org/wiki/Domain_hack">domainhack</a> is an
                unconventional domain name that combines domain levels, especially the Top-Level
                Domain (TLD), to spell out the full "name" or title of the domain. Domainhacks offer
                the ability to produce extremely short domain names.
              </p>
            </div>
            <div className="col">
              <h3 className="app__h3"># Domain-Hack Examples</h3>
              <p>
                This makes them potentially valuable as redirectors, pastebins, base domains from
                which to delegate subdomains and URL shortening services. Popular examples are
                blo.gs, del.icio.us, instagr.am, dub.ai, canv.as.
              </p>
            </div>
            <div className="col">
              <h3 className="app__h3"># The Creator</h3>
              <p>
                Domainhack.me is based on Facebook's <span className="app__span">ReactJS</span> and
                was created by Dmitri Pyslari (
                <a href="https://instagram.com/dmitripyslari">@dmitripyslari</a>), web developer
                from Chisinau.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
